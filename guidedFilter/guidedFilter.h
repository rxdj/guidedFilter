﻿#pragma once
#include <iostream>
#include <string>
#include <opencv2\opencv.hpp>

using namespace std;

class GuidedFilter
{
public:
	GuidedFilter();
	~GuidedFilter();

private:

	cv::Mat runGuidedFilter_Gray(cv::Mat I, cv::Mat P, int type, int radius, double eps);
	cv::Mat runGuidedFilter_Color(cv::Mat I, cv::Mat P, int type, int radius, double eps);

public:
	cv::Mat myGuidedFilter_GrayGuided(cv::Mat guidedImg, cv::Mat inputImg, int radius, double eps);
	cv::Mat myGuidedFilter_ColorGuided(cv::Mat guidedImg, cv::Mat inputImg, int radius, double eps);


};