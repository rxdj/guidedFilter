### guidedFilter引导图滤波算法C++实现


- 输入：  
guidedImg　　----引导图像,单通道或者三通道  
inputImg　　----输入待滤波图像，单通道或者三通道  
r　　　　　　----滤波窗口半径  
eps　　　　　----截断值eps

- 输出：  
outputImg　　----引导图滤波后图像
